<?php
  session_start();
  include "_database.php";

  if(isset($_SESSION["userid"])){
    $userid = $_SESSION["userid"];
  } else if (isset($_COOKIE["userid"])){
     $userid = $_COOKIE["userid"];
     $_SESSION["userid"] = $userid;
     setcookie("userid", $userid, time() + (86400 * 30), "/"); 
  // } else if (isset($_COOKIE["userid"])){
  //    $userid = $_COOKIE["userid"];
  //    $_SESSION["userid"] = $userid;
  //    setcookie("userid", $userid, time() + (86400 * 30), "/"); 
} else{
    $userid = 0;
  }

  // If there is new tweet submission, persist it.
  if (isset($_POST["title"]) && isset($_POST["content"]) && $userid > 0)
  {
    $stmt = mysqli_stmt_init($conn);
    $insert_sql = "INSERT INTO statuses (title, content, author_id) VALUES (?, ?, ?)";

    if (mysqli_stmt_prepare($stmt, $insert_sql))
    {
      mysqli_stmt_bind_param($stmt, "ssi", $_POST["title"], $_POST["content"], $userid);
      mysqli_execute($stmt);
    }

  }


  // Fetch all the tweets, in descending order so newest tweets are shown first.
  $sql = "SELECT * FROM statuses INNER JOIN user ON statuses.author_id=user.id ORDER BY statuses.id DESC";
  $result = mysqli_query($conn, $sql); 

  // This is called ternary operator, a shortform of usual if/else statement :)
  // Read this: http://davidwalsh.name/php-shorthand-if-else-ternary-operators
  $color = isset($_GET["color"]) ? $_GET["color"] : "";
  $section = isset($_GET["section"]) ? $_GET["section"] : "";
  $picture = array();
$image_sql = "SELECT * FROM image";
$Image_result = mysqli_query($conn, $image_sql); 
while ($row = mysqli_fetch_assoc($Image_result)){
  array_push($picture, $row["name"]);
}

?><!doctype html>
<html>
  <head>
    <title>Awesome Site - Home</title>
    <link rel="stylesheet" type="text/css" href="awesome-site.css">
    <style>
      #header {
        background: <?= $color ?>;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div id="header">
        <h1>Joey's Awesome Site</h1>
      </div>
      <div id="navigation">
        <div class="left">
          <a href="index.php?section=home" class="link">Home</a>
          <a href="index.php?section=about" class="link">About</a>
        </div>
        <div class="right">
          <form class="change-header-color" method="get" action="index.php">
            Change header color:
            <input type="text" name="color" placeholder="New Header Color">
            <input type="submit">
          </form>
        </div>
      </div>
      <?php
        if($section == "home" || $section == "") {
          include "_home.php";
        } else if ($section == "about") {
          include "_about.php";
        }
      ?>
    </div>
  </body>
</html>

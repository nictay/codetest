<?php  
// Upload image code
  include("_database.php");
  if(isset($_POST["imageFile"])) {
    $target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
  $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

      // You can click the function name to find out what it does.
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
          echo "File is an image - " . $check["mime"] . ".";
          $uploadOk = 1;
      } else {
          echo "File is not an image.";
          $uploadOk = 0;
      }

      if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $stmt = mysqli_stmt_init($conn);
                $insert_image_sql = "INSERT INTO image (name) VALUES (?)";
                if (mysqli_stmt_prepare($stmt, $insert_image_sql))
                {
                    mysqli_stmt_bind_param($stmt, "s", $_FILES["fileToUpload"]["name"]);
                    mysqli_execute($stmt);
                }
                
              header('Location: index.php');
          } else {
              echo "Sorry, there was an error uploading your file.";
          }
      }
  }

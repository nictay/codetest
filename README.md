# Awesome Site

## Intro to Web Dev Workshop by Hackerspace MMU

### Sessions

- HTML and CSS by Joey
- PHP by Hii - [Guide](https://drive.google.com/open?id=1XuJhqsKAR-smLXUNKVmXu_WBP7PfheDPBeWkXB4dcSM)
- PHP, MySQL and CRUD by Anonoz - [Guide](https://drive.google.com/open?id=1opq09Nfe7Pffj20H7XD3ruJjPK7MjfGHUqSgktDKXDU)
- Session 4 by Shaun - coming soon

<?php
	session_start();
	$cookie_name = "userid";
    $cookie_value = $_SESSION["userid"];
    setcookie($cookie_name, $cookie_value, time() - (86400 * 30), "/"); 
	session_destroy();

	$userid = 0;
	header('Location: index.php');
?>